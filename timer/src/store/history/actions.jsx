import {ADD_HISTORY} from "../types"

export const addHistory = task =>{
    return{
        type: ADD_HISTORY,
        payload: {task}
    }
}
