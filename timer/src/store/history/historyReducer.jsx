
const initialState = ['5', '5', '5']

const historyReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_HISTORY':
            return [
                ...state,
                action.payload.task
            ]

        default:
            return state
    }
}
export default historyReducer