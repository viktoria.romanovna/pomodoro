import {combineReducers} from "redux";
import inputReducer from "./input/inputReducer";
import historyReducer from "./history/historyReducer";

const reducer = combineReducers({
     tasks: inputReducer,
     finishedTasks: historyReducer
})

export default reducer;
