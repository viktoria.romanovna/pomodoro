
const initialState = []

const inputReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_TASK':
            return [
                ...state,
                action.payload.task
            ]

        default:
            return state
    }
}
export default inputReducer