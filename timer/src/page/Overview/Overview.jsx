import {useSelector} from "react-redux";
import './Overview.css'

const Overview = () =>{

    const finishTask = useSelector(state => state.finishedTasks)

    const statTasks = finishTask.reduce((acc, task)=>{
        if(acc.find(i=> i.name  === task)) {
            const taskIndex = acc.findIndex(i=> i.name  === task)
            acc[taskIndex].count = acc[taskIndex].count +1
        } else {
            let taskObj = {
                name: task,
                count: 1
            }
            acc.push(taskObj)

        }
        return acc
    }, [])

    console.log(statTasks);

    return(
        <div>
            <h1 className='center'>Статистика</h1>
            <ul>
                {statTasks.map(task=>(<li>{task.name} - {task.count}</li>))}
            </ul>
        </div>
    )
}

export default Overview