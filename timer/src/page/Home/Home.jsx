import Button from "../../components/Button/Button";
import Timer from "../../components/Timer/Timer";
import React, {useRef} from "react";
import {useSelector, useDispatch} from "react-redux";
import {addTask} from "../../store/input/actions";
import {addHistory} from "../../store/history/actions";
import './Home.css'


const Home = () => {

    const dispatch = useDispatch()

    const inputTask = useRef(null)

    const tasks = useSelector(function (state){
        return state.tasks
    })

    let newTask = tasks.map((task, index)=>(
            <option key={index} value={task}>{task}</option>
        ))

    const handleFinish = () =>{
        dispatch(addHistory(inputTask.current.value))
        console.log('finish')

    }

    console.log(tasks);


    return (
        <>
            <h1 className="center">Старт задач</h1>
            <div className="input-task">
                <input className='input' ref={inputTask}/>
                <Button text="add" onClick={()=>{
                    dispatch(addTask(inputTask.current.value));console.log('click');
                    }}/>
            </div>
            <div className="input-task">
                <select className="select input">
                    <option value="" selected disabled>select task</option>
                    {newTask}
                </select>
            </div>
            <div>
                <Timer onFinish={handleFinish}/>
            </div>
        </>

    )
}

export default Home