import {useDispatch, useSelector} from "react-redux";
import {addHistory} from "../../store/history/actions";
import {useEffect, useRef} from "react";
import './history.css'

const History = () =>{
    const dispatch = useDispatch()

    const finishedTasks = useRef(null)

    const finishTask = useSelector(state => state.finishedTasks)
    console.log('finishTask', finishTask)

    const historyTasks = finishTask.map(i => {
        return(
                <li>{i}</li>
            )
    })

return(
    <div>
        <h1 className='center'>История выполненных задач</h1>
        <ul>
            {historyTasks}
        </ul>
    </div>
)
}

export default History