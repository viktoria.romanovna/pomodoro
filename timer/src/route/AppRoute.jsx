import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';

import Home from "../page/Home/Home";
import Overview from "../page/Overview/Overview";
import History from "../page/History/History";

const AppRoutes = () => {
    return (
        <div className="app-routes">
            <Switch>
                <Redirect exact from="/" to="/home"/>

                <Route exact
                       path="/home"
                       render={(routerProps) => (
                           <Home {...routerProps} />
                       )}
                />

                <Route exact
                       path="/overview"
                       render={(routerProps) => (
                           <Overview {...routerProps} />
                       )}
                           />

                <Route exact
                       path="/history"
                           render={(routerProps) => (
                               <History {...routerProps} />
                           )}
                               />
                />

            </Switch>
        </div>
    );
}

export default AppRoutes
