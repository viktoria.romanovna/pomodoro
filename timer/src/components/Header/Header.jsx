import React from 'react';
import {NavLink} from 'react-router-dom';
import './Header.css'


const Header = () => {
    return (
        <ul className="header">
            <li>
                <NavLink className="header-item"to='/home'>
                    home
                </NavLink>
            </li>

            <li>
                <NavLink className="header-item" to='/overview'>
                    overview
                </NavLink>
            </li>

            <li>
                <NavLink className="header-item" to='/history'>
                    history
                </NavLink>
            </li>
        </ul>
    );

}

export default Header;

