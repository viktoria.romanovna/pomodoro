import React, {useEffect, useState} from 'react'
import Button from "../Button/Button";
import './Timer.css'

let intervalId = null

const Timer = ({onFinish}) => {
    const [counter, setCounter] = useState(5);

    const startTimer = () => {
            intervalId = setInterval(() => {
                    setCounter((prevCounter) => prevCounter - 1)
            }, 1000
        )}

    useEffect(() => {
        if (counter <= 0) {
            onFinish()
            pauseTimer()
            setTimeout(() => {
                setCounter(5)
            }, 1500)

        }
    }, [counter, onFinish])

    const pauseTimer =() => {
        clearInterval(intervalId)
    }

    return (
        <div className="timer">

            <Button text="start" onClick={startTimer}/>
            <div className='counter'>Countdown: {counter > 0 ? counter : 'Finished'}</div>
            <div className="def-button">
                <Button text="Pause" onClick={pauseTimer}/>
                <Button text="Resume" onClick={startTimer}/>
            </div>

        </div>
    );
}
export default Timer