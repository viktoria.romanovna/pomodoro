import React from 'react'
import './button.css'

const Button = (props) => {
    const {text, onClick} = props
    return (
        <div>
            <button className='button' onClick={onClick}>{text}</button>
        </div>
    )
}
export default Button;