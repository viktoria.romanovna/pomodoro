import React from 'react';
import './App.css';
import Header from "./components/Header/Header";
import Home from "./page/Home/Home";
import History from "./page/History/History";
import AppRoutes from "./route/AppRoute";

function App() {
  return (
      <div className='general'>
          <Header/>
          <AppRoutes/>
      </div>
  );
}

export default App;
